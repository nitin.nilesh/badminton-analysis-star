#!/usr/bin/env python
import sys
import termios
import contextlib
import time
from threading import Thread
import argparse
sys.path.append('PyTorch_YOLOv3')
import PyTorch_YOLOv3 as ptyolo
import cv2
from tqdm import tqdm
import numpy as np

# Video frame rate in FPS
FRAME_RATE = 25

# Video snippet duration in seconds
SNIPPET_DURATION = 10

# Max frames to be processed in a single pass
CAPTURE_FRAMES = FRAME_RATE * SNIPPET_DURATION

pts_dst = np.array([[263, 94], [934, 94], [934, 1589], [263, 1589]])


@contextlib.contextmanager
def raw_mode(file):
    old_attrs = termios.tcgetattr(file.fileno())
    new_attrs = old_attrs[:]
    new_attrs[3] = new_attrs[3] & ~(termios.ECHO | termios.ICANON)
    try:
        termios.tcsetattr(file.fileno(), termios.TCSADRAIN, new_attrs)
        yield
    finally:
        termios.tcsetattr(file.fileno(), termios.TCSADRAIN, old_attrs)

process_id = 0

player_1_distances = []
player_2_distances = []

rally_locs1 = []
rally_locs2 = []

set_locs1 = []
set_locs2 = []

set_dist1 = 0
set_dist2 = 0

to_write = True

# def yolo():
#     print("Detecting image----------")
#     time.sleep(1)
#     return (1,2),(3,4)

def count(vid):
    global distance


    startFrame = 1
    lastProcessedFrame = 100

    vidObj = cv2.VideoCapture(vid)
    lastProcessedFrame = int(vidObj.get(cv2.CAP_PROP_FRAME_COUNT))-200 #105000


    while(True):
        # time.sleep(2)
        vidObj = cv2.VideoCapture(vid)
        totalFrames = int(vidObj.get(cv2.CAP_PROP_FRAME_COUNT))
        print("Total Frames : " + str(totalFrames))
        if totalFrames <= lastProcessedFrame:
            break

    # REMOVE THIS FOR RUNNING COMPLETE VIDEO
    # if lastProcessedFrame > 3000:
    #     break
    
        endFrame = min(CAPTURE_FRAMES + lastProcessedFrame, totalFrames)
        startFrame = lastProcessedFrame + 1
        lastProcessedFrame = endFrame
    
    # startFrame = 190000
        vidObj.set(1, startFrame)

        for idx in tqdm(range(endFrame - startFrame + 1)):
            fc = int(vidObj.get(cv2.CAP_PROP_FRAME_COUNT))
            if idx > (fc) - 50:
                time.sleep(1)
            try:
                success, img = vidObj.read()
                if idx == 100:
                    cv2.imwrite("output.jpg", img)
                if success == False:
                    break;
                    
            except:
                break

            # if(to_write):
            #     print("Reading rally")
            # # else:
            #     # print("Not reading rally")    

            if to_write and idx %4==0:
                a,b = ptyolo.getFrameLocations.run(img, opt)
                if int(a[0]) != 0 and int(a[1]) != 0:
                    rally_locs1.append(a)
                    rally_locs2.append(b)
            # else:
            #     # time.sleep(1)
            #     # print("Not Detecting-----------")

def main():
    # print('exit with ^C or ^D')
    global to_write
    with raw_mode(sys.stdin):
        try:
            while True:

                ch = sys.stdin.read(1)
                string = '%02x' % ord(ch)
                if string == "31":
                    to_write = True
                    return 0

                if string == "32":
                    to_write = False
                    return 0

                if string == "33":
                    to_write = False
                    return 1

                if not ch or ch == chr(4):
                    break
                print('%02x' % ord(ch))
        except (KeyboardInterrupt, EOFError):
            pass


if __name__ == '__main__':
    
        # distance = 0

    parser = argparse.ArgumentParser()
    parser.add_argument('--video_path', type=str, required=True, help='path to video')
    # parser.add_argument('--image_path', type=str, required=True, help='path to image')
    parser.add_argument('--config_path', type=str, default='PyTorch_YOLOv3/config/yolov3.cfg', help='path to model config file')
    parser.add_argument('--weights_path', type=str, default='PyTorch_YOLOv3/checkpoints/100_0.716667.weights', help='path to weights file')
    parser.add_argument('--class_path', type=str, default='PyTorch_YOLOv3/data/badminton.names', help='path to class label file')
    parser.add_argument('--conf_thres', type=float, default=0.8, help='object confidence threshold')
    parser.add_argument('--nms_thres', type=float, default=0.1, help='iou thresshold for non-maximum suppression')
    parser.add_argument('--batch_size', type=int, default=8, help='size of the batches')
    parser.add_argument('--n_cpu', type=int, default=1, help='number of cpu threads to use during batch generation')
    parser.add_argument('--img_size', type=int, default=416, help='size of each image dimension')
    parser.add_argument('--use_cuda', type=bool, default=True, help='whether to use cuda if available')
    opt = parser.parse_args()
    # img = cv2.imread(opt.image_path)
    vid = opt.video_path
    distance = 0
    process = Thread(target=count, args=(vid,))
    # process = Process(target=count, args=())
    process.start()
    while main() == 0:
        print("Rally distances : ----------")
        dist1 = ptyolo.generateHeatmap.calcTotalDistance(rally_locs1, pts_dst)
        dist2 = ptyolo.generateHeatmap.calcTotalDistance(rally_locs2, pts_dst)
        print("Player 1 locs " + str(rally_locs1))
        print("Player 2 locs " + str(rally_locs2))
        print("Player 1 distance " + str(dist1))
        print("Player 2 distance " + str(dist2))
        set_locs1 = set_locs1 + rally_locs1
        set_locs2 = set_locs2 + rally_locs2

        set_dist1 = set_dist1 + dist1
        set_dist2 = set_dist2 + dist2

        rally_locs1 = []
        rally_locs2 = []
    print("Final distances : -----------")
    print("Player 1 set distance " + str(set_dist1))
    print("Player 2 set distance " + str(set_dist2))
    
