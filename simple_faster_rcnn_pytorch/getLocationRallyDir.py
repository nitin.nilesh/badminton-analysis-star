import os
import torch as t
from utils.config import opt
from model import FasterRCNNVGG16
from trainer import FasterRCNNTrainer
from data.util import  read_image
from utils import array_tool as at
import cv2
import numpy as np
from tqdm import tqdm
import argparse
import generateHeatmap as heatmap
import pdb

'''Given a rally directory of images, it gives locations of the players in the following format.
frame_no  y1 x1 y2 x2 q1 p1 q2 p2'''
faster_rcnn = FasterRCNNVGG16()
trainer = FasterRCNNTrainer(faster_rcnn).cuda()
trainer.load('simple_faster_rcnn_pytorch/checkpoints/fasterrcnn_06231505_0.9991173874669023')
opt.caffe_pretrain=False # this model was trained from torchvision-pretrained model

def get_pt_pb(loc_set):

    y2 = loc_set[3]
    q2 = loc_set[7]

    if q2 > y2:
        return loc_set
    else:
        loc_set = [loc_set[4], loc_set[5], loc_set[6], loc_set[7], loc_set[0], loc_set[1], loc_set[2], loc_set[3]]
        return loc_set

def location(img_path, prev_loc_set):
    '''
    Adding Previous Lcoation method, if FASTER RCNN is unable to find location for this frame then
    use previous frame location for output. It works cause for subsequent frames locations doesn't change much.
    '''
    img = read_image(img_path)
    img = t.from_numpy(img)[None]

    _bboxes, _labels, _scores =trainer.faster_rcnn.predict(img,visualize=True)
    img_np = at.tonumpy(img[0]).transpose((1,2,0)).astype(np.uint8).copy()
    loc_set = []
    if len(_bboxes[0]) == 2:
        for i in range(at.tonumpy(_bboxes[0]).shape[0]):
            y1,x1,y2,x2=at.tonumpy(_bboxes[0])[i][0],at.tonumpy(_bboxes[0])[i][1],at.tonumpy(_bboxes[0])[i][2],at.tonumpy(_bboxes[0])[i][3]
            y1, x1, y2, x2 = round(y1), round(x1), round(y2), round(x2)
            # cv2.rectangle(img_np,(x1,y1),(x2,y2),(0,0,255),2)
            # cv2.imwrite('test.jpg', img_np)
            loc_set.extend([x1,y1,x2,y2])
    else:
        return prev_loc_set

    loc_set = get_pt_pb(loc_set)
    return loc_set


def get_location_and_heatmap(rally, loc_store_path, SKIP):
    # pdb.set_trace()
    rally_name = os.path.abspath(rally).split('/')[-1]
    if not os.path.exists(loc_store_path):
        os.mkdir(loc_store_path)

    im_list = sorted(os.listdir(rally), key=lambda p: int(p[:-4]))
    rally_path = os.path.join(loc_store_path, rally_name+'output')

    if not os.path.exists(rally_path):
        os.mkdir(rally_path)

    homo_locs_path = os.path.join(rally_path, rally_name+'.txt')
    locs_data = []
    prev_loc_set = [0]*8
    for idx, i in enumerate(tqdm(im_list)):
        if idx%SKIP == 0:
            loc_set = location(rally+'/'+i, prev_loc_set)
            prev_loc_set = loc_set[:]
            locs_data.append(loc_set)
    heatmap_img = heatmap.get_homography_points(locs_data, homo_locs_path)
    # cv2.imwrite(os.path.join(rally_path, 'heatmap'+rally_name+'.jpg'), heatmap_img)
    p1dist, p2dist = heatmap.calculate_distance(homo_locs_path)
    print("Processing complete for Rally : " + str(rally) + "\n\n")

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--rally_path', required=True, help="Path for the Rally directory")
    parser.add_argument('--loc_store_path', required=True, help="Path to store the locations")
    opt = parser.parse_args()
    SKIP=1
    get_location_and_heatmap(opt.rally_path, opt.loc_store_path, SKIP)

