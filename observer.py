import os
import time
import argparse
from torch.multiprocessing import Process, set_start_method
import sys
sys.path.append('simple_faster_rcnn_pytorch')

from simple_faster_rcnn_pytorch import  getLocationRallyDir


def run():
    currentRally = 1
    if initial_rally is not None:
        currentRally = initial_rally

    if opt.multi is not None:
        multiProcessing = True
    else:
        multiProcessing = False

    t1 = time.time()

    while True:
        time.sleep(3)
        if os.path.isdir(rally_path_prefix + str(currentRally + 1)):
            # doAnalysis(currentRally)
            print("Processing started for Rally : " + str(currentRally))
            if multiProcessing:
                p = Process(target=getLocationRallyDir.get_location_and_heatmap,
                            args=(rally_path_prefix + str(currentRally), output_path, SKIP, ))
                p.start()
            else:
                getLocationRallyDir.get_location_and_heatmap(rally_path_prefix + str(currentRally), output_path, SKIP)
            currentRally += 1
        else:
            print("No new rallies found")
            print("Last rally analyzed : Rally " + str(currentRally - 1))
            print("Total time spent : " + str(time.time() - t1) + "\n\n")
            # print(os.path.isdir("data/rally" + str(currentRally + 1)))


def doAnalysis(currentRally):
    time.sleep(15)
    print('parent process:', os.getppid())
    print('process id:', os.getpid())
    print("Total time spent : " + str(time.time() - t1))
    print("****Analysis complete for Rally : " + str(currentRally) + "\n\n")





if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--rally_folder_path', type=str, help='path to the rally frames master folder', required=True)
    parser.add_argument('--output_path', type=str, help='path to the output folder', required=True)
    parser.add_argument('--multi', type=bool, help='Whether to use Multiprocessing', required=False)
    parser.add_argument('--start_from', type=int, help='starting rally', required=False)
    parser.add_argument('--skip_frames', type=int, help='Number of frames to skip')
    opt = parser.parse_args()

    SKIP = opt.skip_frames
    folder_path = opt.rally_folder_path
    output_path = opt.output_path
    initial_rally = opt.start_from
    rally_path_prefix = folder_path + "rally"
    set_start_method('forkserver', force=True)
    run()








