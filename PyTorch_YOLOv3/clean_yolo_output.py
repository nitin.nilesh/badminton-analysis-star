import numpy as np
import pdb

def bb_intersection_over_union(boxA, boxB):
    # determine the (x, y)-coordinates of the intersection rectangle
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])

    # compute the area of intersection rectangle
    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)

    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
    boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = interArea / float(boxAArea + boxBArea - interArea)

    # return the intersection over union value
    return iou

def get_iou(bb1, bb2):
    """
    Calculate the Intersection over Union (IoU) of two bounding boxes.

    Parameters
    ----------
    bb1 : dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x1, y1) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner
    bb2 : dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x, y) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner

    Returns
    -------
    float
        in [0, 1]
    """
    assert bb1[0] < bb1[2]
    assert bb1[1] < bb1[3]
    assert bb2[0] < bb2[2]
    assert bb2[1] < bb2[3]

    # determine the coordinates of the intersection rectangle
    x_left = max(bb1[0], bb2[0])
    y_top = max(bb1[1], bb2[1])
    x_right = min(bb1[2], bb2[2])
    y_bottom = min(bb1[3], bb2[3])

    if x_right < x_left or y_bottom < y_top:
        return 0.0

    # The intersection of two axis-aligned bounding boxes is always an
    # axis-aligned bounding box
    intersection_area = (x_right - x_left) * (y_bottom - y_top)

    # compute the area of both AABBs
    bb1_area = (bb1[2] - bb1[0]) * (bb1[3] - bb1[1])
    bb2_area = (bb2[2] - bb2[0]) * (bb2[3] - bb2[1])

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = intersection_area / float(bb1_area + bb2_area - intersection_area)
    assert iou >= 0.0
    assert iou <= 1.0
    return iou

def get_two_boxes(box1, box2, box3, pts_src, img, opt):

    # pdb.set_trace()

    def get_correct_corners(box):

        x1, y1, x2, y2 = box[0], box[1], box[2], box[3]
        # The amount of padding that was added
        pad_x = max(img.shape[0] - img.shape[1], 0) * (opt.img_size / max(img.shape))
        pad_y = max(img.shape[1] - img.shape[0], 0) * (opt.img_size / max(img.shape))
        # Image height and width after padding is removed
        unpad_h = opt.img_size - pad_y
        unpad_w = opt.img_size - pad_x

        box_h = ((y2 - y1) / unpad_h) * img.shape[0]
        box_w = ((x2 - x1) / unpad_w) * img.shape[1]
        y1 = ((y1 - pad_y // 2) / unpad_h) * img.shape[0]
        x1 = ((x1 - pad_x // 2) / unpad_w) * img.shape[1]

        x2 = x1 + box_w
        y2 = y1 + box_h
        return [x1, y1, x2, y2] + box[4:].tolist()

    boxes = [box1, box2, box3]
    # for box in [box1, box2, box3]:
    #     box_new = get_correct_corners(box)
    #     if (box_new[0] > pts_src[3][0] and box_new[0] < pts_src[2][0]):
    #         boxes.append(box)
    if len(boxes) > 2: 
        box1 = boxes[0]
        box2 = boxes[1]
        box3 = boxes[2]
        
        iou = bb_intersection_over_union(box1[:4], box2[:4])
        if iou > 0.5:
            boxA = box1
            boxB = box3
        else:
            boxA = box1
            boxB = box2

        return boxA, boxB
    elif len(boxes)==2:
        boxA = boxes[0]
        boxB = boxes[1]

        return boxA, boxB

def label_box(boxA, boxB, height, width):

    # boxA_mid = np.array([(boxA[0] + boxA[2])//2, boxA[3]])
    # boxB_mid = np.array([(boxB[0] + boxB[2])//2, boxB[3]])

    # img_mid = np.array([height, width//2])
    # dist_boxA = np.linalg.norm(img_mid - boxA_mid)
    # dist_boxB = np.linalg.norm(img_mid - boxB_mid)

    dist_boxA = boxA[3]
    dist_boxB = boxB[3]

    if dist_boxB > dist_boxA:
        boxA[-1] = 0
        boxB[-1] = 1
    else:
        boxA[-1] = 1
        boxB[-1] = 0
    return boxA, boxB




    