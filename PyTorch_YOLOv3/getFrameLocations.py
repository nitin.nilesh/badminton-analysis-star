from __future__ import division

from models import *
from utils.utils import *
from utils.datasets import *
from clean_yolo_output import *
import generateHeatmap as gh

import os
import sys
import time
import datetime
import argparse
import pdb
import cv2

import torch
from torch.utils.data import DataLoader
from torchvision import datasets
from torch.autograd import Variable
import torch.utils.data as utils

from skimage.transform import resize
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.ticker import NullLocator

config_path = 'PyTorch_YOLOv3/config/yolov3.cfg'
weights_path = 'PyTorch_YOLOv3/checkpoints/100_0.716667.weights'
img_size = 416
class_path = 'PyTorch_YOLOv3/data/badminton.names'
cuda = torch.cuda.is_available()

# Set up model
model = Darknet(config_path, img_size=img_size)
model.load_weights(weights_path)
classes = load_classes(class_path) # Extracts class labels from file
img_shape = (img_size, img_size)
Tensor = torch.cuda.FloatTensor if cuda else torch.FloatTensor
conf_thres = 0.9
nms_thres = 0.1
pts_src = gh.pts_src # image four corners

if cuda:
    model.cuda()

model.eval() # Set in evaluation mode

def order_pt_pb(tl):
    try:
        x1, y1, x2, y2 = tl[0], tl[1], tl[2], tl[3]
        p1, q1, p2, q2 = tl[5], tl[6], tl[7], tl[8]
        cls = tl[4]
        if cls == 0:
            return [x1, y1, x2, y2, p1, q1, p2, q2]
        else:
            return [p1, q1, p2, q2, x1, y1, x2, y2]
    except IndexError:
        pass

def run(img, opt):
    """img is numpy array."""
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    h, w, _ = img.shape
    dim_diff = np.abs(h - w)
    # Upper (left) and lower (right) padding
    pad1, pad2 = dim_diff // 2, dim_diff - dim_diff // 2
    # Determine padding
    pad = ((pad1, pad2), (0, 0), (0, 0)) if h <= w else ((0, 0), (pad1, pad2), (0, 0))
    # Add padding
    input_img = np.pad(img, pad, 'constant', constant_values=127.5) / 255.
    # Resize and normalize
    input_img = resize(input_img, (*img_shape, 3), mode='reflect')
    # Channels-first
    input_img = np.transpose(input_img, (2, 0, 1))
    # As pytorch tensor
    input_img = torch.from_numpy(input_img).float()
    input_img.unsqueeze_(0)
    input_img = Variable(input_img.type(Tensor))
    
    with torch.no_grad():
        detection = model(input_img)
        detection = non_max_suppression(detection, 2, conf_thres, nms_thres)
    
    # The amount of padding that was added
    pad_x = max(img.shape[0] - img.shape[1], 0) * (img_size / max(img.shape))
    pad_y = max(img.shape[1] - img.shape[0], 0) * (img_size / max(img.shape))
    # Image height and width after padding is removed
    unpad_h = img_size - pad_y
    unpad_w = img_size - pad_x
    # print(detection)
    detection = detection[0]
    if detection is not None:
        if len(detection) > 2:
            # print("Applying Correctification")
            bbox1 = detection[0].cpu().data.numpy()
            bbox2 = detection[1].cpu().data.numpy()
            bbox3 = detection[2].cpu().data.numpy()
            boxA, boxB = get_two_boxes(bbox1, bbox2, bbox3, pts_src, img, opt)
            boxA, boxB = label_box(boxA, boxB, img.shape[0], img.shape[1])
            detection = torch.Tensor(np.vstack((boxA, boxB)))
    # pdb.set_trace()
    # Draw bounding boxes and labels of detection
    if detection is not None and len(detection) > 1:
        unique_labels = detection[:, -1].cpu().unique()
        n_cls_preds = len(unique_labels)
        temp_locs = []
        for x1, y1, x2, y2, conf, cls_conf, cls_pred in detection:
            # print(cls_pred)
            # print ('\t+ Label: %s, Conf: %.5f' % (classes[int(cls_pred)], cls_conf.item()))

            # Rescale coordinates to original dimensions
            box_h = ((y2 - y1) / unpad_h) * img.shape[0]
            box_w = ((x2 - x1) / unpad_w) * img.shape[1]
            y1 = ((y1 - pad_y // 2) / unpad_h) * img.shape[0]
            x1 = ((x1 - pad_x // 2) / unpad_w) * img.shape[1]
            x2 = x1 + box_w
            y2 = y1 + box_h

            # print(x1, y1, x2, y2, cls_pred)
            temp_locs.extend([x1, y1, x2, y2, cls_pred])

        temp_locs = order_pt_pb(temp_locs)
        p1, p2 = gh.get_homography_point(temp_locs)
        return p1, p2
    else:
        return (0,0), (0,0)
        

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--image_path', type=str, required=True, help='path to image')
    parser.add_argument('--config_path', type=str, default='config/yolov3.cfg', help='path to model config file')
    parser.add_argument('--weights_path', type=str, default='checkpoints/100_0.716667.weights', help='path to weights file')
    parser.add_argument('--class_path', type=str, default='data/badminton.names', help='path to class label file')
    parser.add_argument('--conf_thres', type=float, default=0.8, help='object confidence threshold')
    parser.add_argument('--nms_thres', type=float, default=0.1, help='iou thresshold for non-maximum suppression')
    parser.add_argument('--batch_size', type=int, default=8, help='size of the batches')
    parser.add_argument('--n_cpu', type=int, default=1, help='number of cpu threads to use during batch generation')
    parser.add_argument('--img_size', type=int, default=416, help='size of each image dimension')
    parser.add_argument('--use_cuda', type=bool, default=True, help='whether to use cuda if available')
    opt = parser.parse_args()
    print(opt)
    img = cv2.imread(opt.image_path)
    # img = np.array(Image.open(opt.image_path))
    p1, p2 = run(img)
    print(p1, p2)

    

    # imgs = []           # Stores image paths
    # img_detections = [] # Stores detections for each image index
    # pts_src = gh.pts_src # image four corners


    # print ('\nPerforming object detection:')

    # start_time = time.time()
    # prev_time = time.time()
    # for batch_i, (img_paths, input_imgs) in enumerate(dataloader):
    #     # Configure input
    #     input_imgs = Variable(input_imgs.type(Tensor))

    #     # Get detections
    #     with torch.no_grad():
    #         detections = model(input_imgs)
    #         detections = non_max_suppression(detections, 80, opt.conf_thres, opt.nms_thres)


    #     # Log progress
    #     current_time = time.time()
    #     inference_time = datetime.timedelta(seconds=current_time - prev_time)
    #     prev_time = current_time
    #     print ('\t+ Batch %d, Inference Time: %s' % (batch_i, inference_time))

    #     # Save image and detections
    #     imgs.extend(img_paths)
    #     img_detections.extend(detections)

    # print('total_time', time.time() - start_time)

    # # Bounding-box colors
    # cmap = plt.get_cmap('tab20b')
    # colors = [cmap(i) for i in np.linspace(0, 1, 20)]

    # # Iterate through images and save plot of detections
    # for img_i, (path, detections) in enumerate(zip(imgs, img_detections)):

    #     print ("(%d) Image: '%s'" % (img_i, path))

    #     # Create plot
    #     img = np.array(Image.open(path))
    #     # plt.figure()
    #     fig, ax = plt.subplots(1)
    #     ax.imshow(img)

    #     # The amount of padding that was added
    #     pad_x = max(img.shape[0] - img.shape[1], 0) * (opt.img_size / max(img.shape))
    #     pad_y = max(img.shape[1] - img.shape[0], 0) * (opt.img_size / max(img.shape))
    #     # Image height and width after padding is removed
    #     unpad_h = opt.img_size - pad_y
    #     unpad_w = opt.img_size - pad_x

    #     # Remove redundant boxes and get the correct labels
    #     # pdb.set_trace()
    #     if len(detections) > 2:
    #         print("Applying Correctification")
    #         bbox1 = detections[0].cpu().data.numpy()
    #         bbox2 = detections[1].cpu().data.numpy()
    #         bbox3 = detections[2].cpu().data.numpy()
    #         boxA, boxB = get_two_boxes(bbox1, bbox2, bbox3, pts_src, img, opt)
    #         boxA, boxB = label_box(boxA, boxB, img.shape[0], img.shape[1])
    #         detections = torch.Tensor(np.vstack((boxA, boxB)))

    #     # pdb.set_trace()
    #     # Draw bounding boxes and labels of detections
    #     if detections is not None:
    #         unique_labels = detections[:, -1].cpu().unique()
    #         n_cls_preds = len(unique_labels)
    #         bbox_colors = random.sample(colors, n_cls_preds)
    #         for x1, y1, x2, y2, conf, cls_conf, cls_pred in detections:
    #             print(cls_pred)
    #             print ('\t+ Label: %s, Conf: %.5f' % (classes[int(cls_pred)], cls_conf.item()))

    #             # Rescale coordinates to original dimensions
    #             box_h = ((y2 - y1) / unpad_h) * img.shape[0]
    #             box_w = ((x2 - x1) / unpad_w) * img.shape[1]
    #             y1 = ((y1 - pad_y // 2) / unpad_h) * img.shape[0]
    #             x1 = ((x1 - pad_x // 2) / unpad_w) * img.shape[1]
    #             print('x1, y1, x2, y2',x1, y1, box_w, box_h)

    #             color = bbox_colors[int(np.where(unique_labels == int(cls_pred))[0])]
    #             # Create a Rectangle patch
    #             bbox = patches.Rectangle((x1, y1), box_w, box_h, linewidth=2, edgecolor=color,
    #                                     facecolor='none')
    #             # Add the bbox to the plot
    #             ax.add_patch(bbox)
    #             # Add label
    #             plt.text(x1, y1, s=classes[int(cls_pred)], color='white', verticalalignment='top',
    #                     bbox={'color': color, 'pad': 0})

    #     # Save generated image with detections
    #     plt.axis('off')
    #     plt.gca().xaxis.set_major_locator(NullLocator())
    #     plt.gca().yaxis.set_major_locator(NullLocator())
    #     # plt.show()
    #     plt.savefig('output/%d.png' % (img_i), bbox_inches='tight', pad_inches=0.0)
    #     plt.close()
