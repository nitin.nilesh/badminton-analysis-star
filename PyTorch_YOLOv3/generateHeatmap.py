import os
import numpy as np
import matplotlib.pyplot as plt
import cv2
import argparse

# Image from gameplay and corners
# Corners are taken in clock-wise direction starting from top-left corner
im_src = plt.imread('/media/star/54d2c14a-ddf4-40f2-a91a-fce68de5c81e/badminton-analysis/PyTorch_YOLOv3/assets/69641.jpg')
pts_src = np.array([[626, 461], [1296, 461], [1400, 945], [515, 945]])

# London 2012 Olympics Court and Corners
# im_dst = cv2.imread('../court_top_vertical.jpg')
# pts_dst = np.array([[106, 134], [681, 142], [683, 1413], [100, 1412]])

# Downloaded Court and corners
# im_dst = cv2.imread('../badminton_court2.png')
# pts_dst = np.array([[20, 20], [274, 20], [274, 578], [20, 578]])

# New IIIT Court and corners
# im_dst = cv2.imread('badminton_court_IIIT.jpg')
# pts_dst = np.array([[200, 72], [710, 72], [710, 1208], [200, 1208]])

# New Non-IIIT Court and corners
im_dst = cv2.imread('/media/star/54d2c14a-ddf4-40f2-a91a-fce68de5c81e/badminton-analysis/PyTorch_YOLOv3/assets/badminton_court_non_IIIT.jpg')
pts_dst = np.array([[263, 94], [934, 94], [934, 1589], [263, 1589]])

def getCourtScalingFactor(courtCorners):

    # court width and height in metres
    courtWidth = 6.1
    courtHeight = 13.4

    horizontal_distance = courtCorners[1][0] - courtCorners[0][0]
    vertical_distance = courtCorners[3][1] - courtCorners[0][1]

    return courtWidth/horizontal_distance, courtHeight/vertical_distance

def calcStepDistance(point1, point2):

    x, y = point1[0], point1[1]
    x_prime, y_prime = point2[0], point2[1]

    vertical_distance = y_prime - y
    horizontal_distance = x_prime - x

    horizontal_scale, vertical_scale = getCourtScalingFactor(pts_dst)

    horizontal_distance = horizontal_distance * horizontal_scale
    vertical_distance = vertical_distance * vertical_scale

    return np.sqrt(horizontal_distance ** 2 + vertical_distance ** 2)

def calcTotalDistance(positions, courtCorners):

    totalDist = 0
    for i in range (len(positions) - 2):
        if(positions[i][0] == 0):
            continue
        stepDist = calcStepDistance(positions[i], positions[i+1])
        totalDist = totalDist + stepDist
    return totalDist


def find_center_point(coords):
    ''' Given a Bbox, it gives the bottom middle point'''

    x1,y1,x2,y2 = coords
    return int((x1+x2)/2), int(y2)

def get_homography_point(loc):
    "loc is a single frame location"
    h, status = cv2.findHomography(pts_src, pts_dst)
    x1,y1,x2,y2 = float(loc[0]),float(loc[1]),float(loc[2]),float(loc[3])
    p1,q1,p2,q2 = float(loc[4]),float(loc[5]),float(loc[6]),float(loc[7])

    p1_loc = find_center_point((x1,y1,x2,y2))
    p2_loc = find_center_point((p1,q1,p2,q2))

    p1_vec = np.float32([p1_loc[0], p1_loc[1]]).reshape(-1, 1, 2)
    p2_vec = np.float32([p2_loc[0], p2_loc[1]]).reshape(-1, 1, 2)

    p1_new_vec = cv2.perspectiveTransform(p1_vec, h) # Converts location from 
    p2_new_vec = cv2.perspectiveTransform(p2_vec, h) # image 1 to image 2

    return (p1_new_vec[0][0][0], p1_new_vec[0][0][1]), (p2_new_vec[0][0][0], p2_new_vec[0][0][1])


def get_homography_points(rally_locs, homo_locs_path):

    # File to save the location
    homography_points = open(homo_locs_path, 'w')
    SKIP = 10

    # Get the 3x3 Homography matrix
    h, status = cv2.findHomography(pts_src, pts_dst)

    for i, loc in enumerate(rally_locs):

        x1,y1,x2,y2 = float(loc[0]),float(loc[1]),float(loc[2]),float(loc[3])
        p1,q1,p2,q2 = float(loc[4]),float(loc[5]),float(loc[6]),float(loc[7])

        p1_loc = find_center_point((x1,y1,x2,y2))
        p2_loc = find_center_point((p1,q1,p2,q2))

        cv2.circle(im_src, (p1_loc[0], p1_loc[1]), 5, (0,0,255), -1)
        cv2.circle(im_src, (p2_loc[0], p2_loc[1]), 5, (0,0,255), -1)
        cv2.namedWindow("source image", cv2.WINDOW_NORMAL)
        cv2.imshow('source image', im_src)

        p1_vec = np.float32([p1_loc[0], p1_loc[1]]).reshape(-1, 1, 2)
        p2_vec = np.float32([p2_loc[0], p2_loc[1]]).reshape(-1, 1, 2)

        # The line below is the saviour. Thanks OpenCV !
        p1_new_vec = cv2.perspectiveTransform(p1_vec, h) # Converts location from 
        p2_new_vec = cv2.perspectiveTransform(p2_vec, h) # image 1 to image 2

        if i % SKIP == 0:
            homography_points.writelines(','.join((str(p1_new_vec[0][0][0]),
                                                   str(p1_new_vec[0][0][1]),
                                                   str(p2_new_vec[0][0][0]),
                                                   str(p2_new_vec[0][0][1]))) + '\n')

        cv2.circle(im_dst, (p1_new_vec[0][0][0], p1_new_vec[0][0][1]), 5, (255,0,0), -1)
        cv2.circle(im_dst, (p2_new_vec[0][0][0], p2_new_vec[0][0][1]), 5, (0,0,255), -1)
        
        # If imshow
        # cv2.namedWindow("Generated Heatmap", cv2.WINDOW_NORMAL)
        # cv2.imshow('Generated Heatmap', im_dst)

    homography_points.close()
    # cv2.imwrite('rally3_heatmap.jpg', im_src)
    # cv2.imwrite('rally3_heatmap_homo.jpg', im_dst)
    # cv2.waitKey(0)

    return im_dst

def calculate_distance(homo_locs_path):

    locs_data = np.loadtxt(homo_locs_path, delimiter=',',dtype=str)
    locsP1 = locs_data[:, :2].astype(float)
    locsP2 = locs_data[:, 2:].astype(float)

    p1_dist = calcTotalDistance(locsP1, pts_dst)
    p2_dist = calcTotalDistance(locsP2, pts_dst)

    print("Player 1 distance : " + str(p1_dist))
    print("Player 2 distance : " + str(p2_dist))

    with open(homo_locs_path, 'a') as f:
        f.writelines(str(p1_dist)+'\n')
        f.writelines(str(p2_dist)+'\n')

    return None


    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--locs', type=str, required=True, help='Path for the rally locations')
    parser.add_argument('--homo_locs', type=str, required=True, help='path for saving homo locs')
    opt = parser.parse_args()
    get_homography_points(opt.locs, opt.homo_locs)
    calculate_distance(opt.homo_locs)


