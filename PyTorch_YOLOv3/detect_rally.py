from __future__ import division

from models import *
from utils.utils import *
from utils.datasets import *
from clean_yolo_output import *
import generateHeatmap as heatmap

import os
import sys
import time
import datetime
import argparse
import pdb
import collections

import torch
from torch.utils.data import DataLoader
from torchvision import datasets
from torch.autograd import Variable

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.ticker import NullLocator

def get_location_and_heatmap(opt):

    os.makedirs(opt.loc_store_path, exist_ok=True)
    
    dataloader = DataLoader(ImageFolder(opt.image_folder, img_size=opt.img_size),
                            batch_size=opt.batch_size, shuffle=False, num_workers=opt.n_cpu)

    classes = load_classes(opt.class_path) # Extracts class labels from file

    Tensor = torch.cuda.FloatTensor if cuda else torch.FloatTensor

    imgs = []           # Stores image paths
    img_detections = [] # Stores detections for each image index
    pts_src = gh.pts_src # image four corners

    print ('\nPerforming object detection:')

    start_time = time.time()
    prev_time = time.time()
    for batch_i, (img_paths, input_imgs) in enumerate(dataloader):
        # Configure input
        input_imgs = Variable(input_imgs.type(Tensor))

        # Get detections
        with torch.no_grad():
            detections = model(input_imgs)
            detections = non_max_suppression(detections, 2, opt.conf_thres, opt.nms_thres)

        # Log progress
        current_time = time.time()
        inference_time = datetime.timedelta(seconds=current_time - prev_time)
        prev_time = current_time
        print ('\t+ Batch %d, Inference Time: %s' % (batch_i, inference_time))

        # Save image and detections
        imgs.extend(img_paths)
        img_detections.extend(detections)

    print('total_time', time.time() - start_time)

    # Iterate through images and save plot of detections
    # pdb.set_trace()
    dict_img_tensor = dict(zip(imgs, img_detections))
    od = collections.OrderedDict(sorted(dict_img_tensor.items(), key=lambda p: int(p[0].split('/')[-1][:-4])))
    locs = []
    # for img_i, (path, detections) in enumerate(zip(imgs, img_detections)):
    for img_i, (path, detections) in enumerate(od.items()):

        print ("(%d) Image: '%s'" % (img_i, path))

        # Create plot
        img = np.array(Image.open(path))

        # The amount of padding that was added
        pad_x = max(img.shape[0] - img.shape[1], 0) * (opt.img_size / max(img.shape))
        pad_y = max(img.shape[1] - img.shape[0], 0) * (opt.img_size / max(img.shape))
        # Image height and width after padding is removed
        unpad_h = opt.img_size - pad_y
        unpad_w = opt.img_size - pad_x

        # Remove redundant boxes and get the correct labels
        # pdb.set_trace()
        if len(detections) > 2:
            print("Applying Correctification")
            bbox1 = detections[0].cpu().data.numpy()
            bbox2 = detections[1].cpu().data.numpy()
            bbox3 = detections[2].cpu().data.numpy()
            boxA, boxB = get_two_boxes(bbox1, bbox2, bbox3, pts_src, img, opt)
            boxA, boxB = label_box(boxA, boxB, img.shape[0], img.shape[1])
            detections = torch.Tensor(np.vstack((boxA, boxB)))

        if detections is not None:
            unique_labels = detections[:, -1].cpu().unique()
            n_cls_preds = len(unique_labels)
            
            temp_locs = []
            for x1, y1, x2, y2, conf, cls_conf, cls_pred in detections:

                # Rescale coordinates to original dimensions
                box_h = ((y2 - y1) / unpad_h) * img.shape[0] # commenting cause
                box_w = ((x2 - x1) / unpad_w) * img.shape[1] # already done in 
                y1 = ((y1 - pad_y // 2) / unpad_h) * img.shape[0] # clean output code
                x1 = ((x1 - pad_x // 2) / unpad_w) * img.shape[1]

                x2 = x1 + box_w
                y2 = y1 + box_h
                print('x1, y1, x2, y2, cls_pred', x1, y1, x2, y2, cls_pred)
                temp_locs.extend([x1, y1, x2, y2])
        locs.append(temp_locs)
    heatmap_img = heatmap.get_homography_points(locs, homo_locs_path)

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--image_folder', type=str, default='data/samples', help='path to dataset')
    parser.add_argument('--config_path', type=str, default='config/yolov3.cfg', help='path to model config file')
    parser.add_argument('--weights_path', type=str, default='checkpoints/100_0.716667.weights', help='path to weights file')
    parser.add_argument('--class_path', type=str, default='data/badminton.names', help='path to class label file')
    parser.add_argument('--conf_thres', type=float, default=0.8, help='object confidence threshold')
    parser.add_argument('--nms_thres', type=float, default=0.1, help='iou thresshold for non-maximum suppression')
    parser.add_argument('--batch_size', type=int, default=8, help='size of the batches')
    parser.add_argument('--n_cpu', type=int, default=4, help='number of cpu threads to use during batch generation')
    parser.add_argument('--img_size', type=int, default=416, help='size of each image dimension')
    parser.add_argument('--loc_store_path', required=True, help="Path to store the locations")
    opt = parser.parse_args()
    print(opt)

    cuda = True

    # Set up model
    model = Darknet(opt.config_path, img_size=opt.img_size)
    model.load_weights(opt.weights_path)
    if cuda:
        model.cuda()
    model.eval() # Set in evaluation mode
    get_location_and_heatmap(opt)




