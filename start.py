
import cv2
from tqdm import tqdm
import time
import argparse
import torch
from torchvision import transforms
from torch.autograd import Variable
import os
import shutil
import scipy.misc as misc
import pdb

# Video frame rate in FPS
FRAME_RATE = 25

# Number of frames to skip
SKIP = 8
# Video snippet duration in seconds
SNIPPET_DURATION = 10

# Max frames to be processed in a single pass
CAPTURE_FRAMES = FRAME_RATE * SNIPPET_DURATION

def create_new_rally():
    dst = os.path.join(match_folder, 'rally' + str(rallyCount))  # Rallies destination location path
    if not os.path.exists(dst):
        os.mkdir(dst)
    files = os.listdir(rally_match)
    frame_numbers = []
    for f in files:
        frame_numbers.append(int(f[:-4]))

    frame_numbers.sort(reverse=True)
    prev = frame_numbers[0]
    for i in range(len(frame_numbers)):
        if prev - frame_numbers[i] > 50:
            break
        else:
            prev = frame_numbers[i]
            shutil.move(rally_match + str(frame_numbers[i]) + ".jpg", dst)
    
    dst_next = os.path.join(match_folder, 'rally' + str(rallyCount+1))  # Rallies destination location path
    os.mkdir(dst_next)

t1 = time.time()
parser = argparse.ArgumentParser()
parser.add_argument('--video_path', type=str, help='path to the video file', required=True)
parser.add_argument('--output_path', type=str, required=True, help='path to store the frames')
opt = parser.parse_args()
vid = opt.video_path

match_name, ext = os.path.splitext(vid)
match_name = os.path.basename(match_name)
match_folder = os.path.join(opt.output_path, match_name)
rally_match = os.path.join(match_folder, 'Temp_frames/')


if not os.path.exists(match_folder):
    os.mkdir(match_folder)
    os.mkdir(rally_match)

startFrame = 1
lastProcessedFrame = 0


model = torch.load('Match2Rally/RallyClassifier_ep10_star.pt') # Loads on PyTorch 0.4
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
data_transforms = transforms.Compose([transforms.ToPILImage(),
                      transforms.Resize(256),
				      transforms.CenterCrop(224),
				      transforms.ToTensor(),
                      transforms.Normalize([0.485,0.456,0.406],[0.229,0.224,0.225])])

nonRallyFrameCount = 0
rallyFrameCount = 0
rallyCount = 1
vidObj = cv2.VideoCapture(vid)
lastProcessedFrame = int(vidObj.get(cv2.CAP_PROP_FRAME_COUNT))-200 #105000
# lastProcessedFrame = 120000

while(True):
    # time.sleep(2)
    vidObj = cv2.VideoCapture(vid)
    totalFrames = int(vidObj.get(cv2.CAP_PROP_FRAME_COUNT))
    print("Total Frames : " + str(totalFrames))
    if totalFrames <= lastProcessedFrame:
        break

    # REMOVE THIS FOR RUNNING COMPLETE VIDEO
    # if lastProcessedFrame > 3000:
    #     break
    
    endFrame = min(CAPTURE_FRAMES + lastProcessedFrame, totalFrames)
    startFrame = lastProcessedFrame + 1
    lastProcessedFrame = endFrame
    
    # startFrame = 190000
    vidObj.set(1, startFrame)
    count = 0
    temp = []
    # pdb.set_trace()

    for idx in tqdm(range(endFrame - startFrame + 1)):
        try:
            success, image = vidObj.read()
            if success == False:
                break
        except:
            break

        # cv2.imwrite('output.jpg', image)
        # image = misc.imresize(image, (720, 1280), interp='bicubic')
        # image = cv2.resize(image, (1280, 720))
        # cv2.imwrite('output1.jpg', image)
        img_tensor = data_transforms(image)
        img_tensor.unsqueeze_(0)
        img_tensor = Variable(img_tensor).to(device)
        output = model(img_tensor)
        _, preds = torch.max(output, 1)
        if preds == 1:
            # cv2.imwrite(os.path.join(rally_match, "%d.jpg" % (startFrame + count)), image)
            temp.append(startFrame + count)
            print("Rally frame")
            rallyFrameCount += 1
            if idx % SKIP == 0:
                cv2.imwrite(os.path.join(rally_match, "%d.jpg" % (startFrame + count)), image)
            if rallyFrameCount > 75:
                nonRallyFrameCount = 0

        elif preds == 0:
            print("Non-Rally frame")
            # cv2.imwrite(os.path.join(non_rally_match, "%d.jpg" % count), image)
            nonRallyFrameCount += 1
            if nonRallyFrameCount > 75 and rallyFrameCount > 75:
                create_new_rally()
                rallyFrameCount = 0
                rallyCount += 1
        else:
            print('Wrong Outcome')

        print("frame # " + str(startFrame + count))
        count += 1

t2 = time.time()
print("Processing complete in " + str(t2-t1) + " secs")
