import os
import sys
import time
import argparse
from Match2Rally import match2rally

parser = argparse.ArgumentParser()
parser.add_argument('--video_path', type=str, help='path to the video file', required=True)
parser.add_argument('--output_path', type=str, required=True, help='path to store the frames')
parser.add_argument('--longines', type=str, default=75, help='Longines of the rally')
parser.add_argument('--difference', type=str, default=50, help='Difference between the rally')
opt = parser.parse_args()
print(opt)

t1 = time.time()
m2r = match2rally.Match2Rally(opt.video_path, opt.output_path)
m2r.classify()
m2r.group()
t2 = time.time()
print(m2r.match_folder)
print("Time taken is:",t2-t1,"seconds")