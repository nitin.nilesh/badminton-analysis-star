# Badminton-Analysis

This repository contains the analysis codes for Badminton.

## How to Start

Simultaneously in different terminals run these commands.
```
python start.py --video_path <path_of_video_file> --output_path <path_to_store_rally_output>

python observer.py --rally_folder_path <path_to_master_rally_folder> --output_path <path_to_store_the_output> --multi <True/False> --skip_frames 1
```

At any point of time run the following command to see player distances
```
python final.py --folder_path <path_to_the_rally_output_master_folder> --delta <number_of_delta_rallies>
```

[Faster RCNN Trained Model weights](https://drive.google.com/file/d/1oLnuVmZR07Dt9UCMoLH2YlImLSkYIP9J/view?usp=sharing)

Keep this trained model in ```simple_faster_rcnn_pytorch/checkpoints``` folder.


[YOLO Trained Model weight](https://drive.google.com/file/d/1BYCoTzuxvHgVYrWeul0xKda0HPGYyD-S/view?usp=sharing)
Keep this trained model in ```PyTorch_YOLOv3/checkpoints``` folder.

## How to run on full recorded video

In start.py, make ```lastProcessedFrame = 0```
In simple_faster_rcnn_pytorch, update ```pts_src``` with rally frame court corners
Run the above defined commands.