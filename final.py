import os
import argparse
import glob
import pdb
import numpy as np


parser = argparse.ArgumentParser()
parser.add_argument('--folder_path', type=str, help='path to the rally output master folder', required=True)
parser.add_argument('--delta', type=int, help='Delta rallies', required=True)
opt = parser.parse_args()

folder_path = opt.folder_path
delta = opt.delta
total_p1_dist = 0
total_p2_dist = 0

folder_list = glob.glob(folder_path+'/rally*')
for f in folder_list:
    fl = os.listdir(f)
    if len(fl) != 0:
        fl_open = open(os.path.join(f, fl[0])).readlines()
        # print(fl_open[-1], fl_open[-2])
        p1_dist = float(fl_open[-1].strip())
        p2_dist = float(fl_open[-2].strip())
        if p1_dist > 0 and p2_dist > 0:
            total_p1_dist += p1_dist
            total_p2_dist += p2_dist

count = len(folder_list)
meanP1 = total_p1_dist/count
meanP2 = total_p2_dist/count

p1_delta = meanP1 * delta
p2_delta = meanP2 * delta

print('Total Player 1 Distance:', total_p1_dist + p1_delta)
print('Total Player 2 Distance:', total_p2_dist + p2_delta)
