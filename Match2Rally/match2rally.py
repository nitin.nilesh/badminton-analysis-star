import os
import sys
import torch
from torchvision import transforms
from torch.autograd import Variable
import numpy as np
import cv2
from tqdm import tqdm
import time
import argparse
import shutil
import torch.multiprocessing as mp

class Match2Rally():
    """
    Given a video, it will classify all the rallies.
    Arguments:
    video_path -- Path of the video file
    output_path -- Path to store the rallies e.g.rally1, rally2 etc.
    longines -- Longiness of the rally
    difference -- Difference between the rally
    """
    def __init__(self, video_path, output_path, longines=75, difference=50):
        super(Match2Rally, self).__init__()
        self.output_path = output_path
        self.vid = video_path
        self.vidObj = cv2.VideoCapture(self.vid)
        self.frames = int(self.vidObj.get(cv2.CAP_PROP_FRAME_COUNT))
        self.model = torch.load('Match2Rally/RallyClassifier_ep10.pt') # Loads on PyTorch 0.4
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.data_transforms = transforms.Compose([transforms.ToPILImage(),
                      transforms.Resize(224),
                      transforms.CenterCrop(224),
                      transforms.ToTensor(),
                      transforms.Normalize([0.485,0.456,0.406],[0.229,0.224,0.225])])
        self.longines = longines
        self.diff = difference
        match_name, ext = os.path.splitext(self.vid)
        match_name = os.path.basename(match_name)
        self.match_folder = os.path.join(self.output_path, match_name)
        self.rally_match = os.path.join(self.match_folder, 'Rally_frames/')
        self.rallies = os.path.join(self.match_folder, 'Rallies/')

    def classify(self):

        print("Total Number of Frames:", self.frames)
        print("Classifying Rally Frames")
        
        if not os.path.exists(self.match_folder):
            os.mkdir(self.match_folder)
        if not os.path.exists(self.rally_match):
            os.mkdir(self.rally_match)
        if not os.path.exists(self.rallies):
            os.mkdir(self.rallies)

        count = 0
        for _ in tqdm(range(self.frames)):
            _, image = self.vidObj.read()
            img_tensor = self.data_transforms(image)
            img_tensor.unsqueeze_(0)
            img_tensor = Variable(img_tensor).to(self.device)
            output = self.model(img_tensor)
            _, preds = torch.max(output,1)

            if preds == 1:
                cv2.imwrite(os.path.join(self.rally_match,"%d.jpg" % count), image)

            count += 1

    def group(self):
        
        frame_names = sorted(os.listdir(self.rally_match), key = lambda p: int(p[:-4]))
        rally_list = []
        temp_list = []

        for i in range(len(frame_names)):
            frame_num = int(frame_names[i][:-4])
            if i < len(frame_names)-1:
                next_frame_num = int(frame_names[i+1][:-4])

                if next_frame_num - frame_num < self.diff: # Difference should be less than 50 frames
                                                           # between two consecutive frames
                    temp_list.append(frame_names[i])
                else:
                    temp_list.append(frame_names[i])
                    if len(temp_list) > self.longines: # Each rally should be atleast 75 frames long.
                        rally_list.append(temp_list)
                    temp_list = []
            else:
                temp_list.append(frame_names[i])
                if len(temp_list) > self.longines:
                    rally_list.append(temp_list)

        print("Total Number of rallies:", len(rally_list))

        rally_count = 1
        for i in range(0, len(rally_list)):

            dst = os.path.join(self.rallies, 'rally'+str(rally_count)) #Rallies destination location path
            os.mkdir(dst) #Create Rally Directory
            
            rally = rally_list[i]
            for frame_name in rally:
                shutil.move(os.path.join(self.rally_match, frame_name), dst)
            rally_count += 1
        
        shutil.rmtree(self.rally_match)
        print("Done")

        

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--video_path', type=str, help='path to the video file', required=True)
    parser.add_argument('--output_path', type=str, required=True, help='path to store the frames')
    parser.add_argument('--longines', type=str, default=75, help='Longines of the rally')
    parser.add_argument('--difference', type=str, default=50, help='Difference between the rally')
    opt = parser.parse_args()
    print(opt)

    t1 = time.time()
    m2r = Match2Rally(opt.video_path, opt.output_path)
    m2r.classify()
    m2r.group()
    t2 = time.time()

    print("Time taken is:",t2-t1,"seconds")

