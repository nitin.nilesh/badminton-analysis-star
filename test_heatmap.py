# Libraries
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import kde
import pdb
import os
import glob
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--folder_path', type=str, help='path to the rally output master folder', required=True)
opt = parser.parse_args()

folder_path = opt.folder_path
p1_total_locs = []
p2_total_locs = []

folder_list = glob.glob(folder_path+'/rally*')
for f in folder_list:
    fl = os.listdir(f)
    if len(fl) != 0:
        fl_open = open(os.path.join(f, fl[0])).readlines()
        for idx, i in enumerate(fl_open):
            if idx <= len(fl_open) - 3:
                i = i.strip()
                i = i.split(',')
                p1_locs = [float(i[0]), float(i[1])]
                p2_locs = [float(i[2]), float(i[3])]
                p1_total_locs.append(p1_locs)
                p2_total_locs.append(p2_locs)


data = np.array(p1_total_locs+p2_total_locs)
x, y = data.T
fig, axes = plt.subplots(ncols=1, nrows=1, frameon=False, figsize=(15, 20), dpi=80)

nbins = 20

k = kde.gaussian_kde(data.T)
xi, yi = np.mgrid[x.min():x.max():nbins*1j, y.min():y.max():nbins*1j]
zi = k(np.vstack([xi.flatten(), yi.flatten()]))

axes.pcolormesh(xi, yi, zi.reshape(xi.shape), shading='gouraud', cmap=plt.cm.BuGn_r)# 

plt.xlim(left = 0)
plt.ylim(top=0)
plt.gca().invert_yaxis()
plt.axis('off')
plt.tight_layout()
plt.autoscale(tight=True)

plt.savefig('xperiment.jpg', bbox_inches='tight', pad_inches=-0.45)
# plt.show()